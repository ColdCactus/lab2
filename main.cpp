#include "Vector.cpp"

void main_menu() {
	cout << "Если вы хотите произвести операцию сложения векторов нажмите 3" << endl;
	cout << "Если вы хотите произвести опреацию умножения вектора на скаляр нажмите 4, после введите значение скаляра, после номер вектора (1 или 2)" << endl;
	cout << "Если вы хотите произвести опреацию нахождения нормы нажмите 5, после задайте номер вектора (1 или 2), у которого вы хотели бы найти норму" << endl;
	cout << "Если вы хотите произвести опреацию скалярного умножения векторов нажмите 6" << endl;
	cout << "Если хотите выйти нажмите 0";
}

void testing_methods() {
		int size = 3;

		int sca_int = 2;
		int mas_0_int[size] = {1, 2, -4};
		int mas_1_int[size] = {6, -1, 1};
		int mas_answer_int_0[size] = {7, 1, -3};
		int mas_answer_int_1[size] = {2, 4, -8};
		int answer_int_0 = 4;
		int answe_int_1 = 0; 
		
		Vector<int> vec_0_int (mas_0_int, size);
		Vector<int> vec_1_int (mas_1_int, size);
		Vector<int>	vec_answer_int_0 (mas_answer_int_0, size);
		Vector<int>	vec_answer_int_1 (mas_answer_int_1, size);

		double sca_double = 1.1;
		double mas_0_double[size] = {1.1, 2.2, -4.4};
		double mas_1_double[size] = {6.3, -1.2, 1.1};
		double mas_answer_double_0[size] = {7.4, 1, -3.3};
		double mas_answer_double_1[size] = {1,21, 2.42, -4.84};
		double answer_double_0 = 5.04083;
		double answe_double_1 = -0.55; 

		Vector<double> vec_0_double (mas_0_double, size);
		Vector<double> vec_1_double (mas_1_double, size);
		Vector<double>	vec_answer_double_0 (mas_answer_double_0, size);
		Vector<double>	vec_answer_double_1 (mas_answer_double_1, size);


		Vector<int> vec_2_int = vec_0_int + vec_1_int;
		if (vec_2_int == vec_answer_int_0) {
			cout << "сложение векторов (целые) - SUCCESS" << endl; 
		}

		Vector<int> vec_3_int = vec_0_int * sca_int;
		if (vec_3_int == vec_answer_int_1) {
			cout << "умножение вектора на скаляр (целые) - SUCCESS" << endl; 
		}

		int check_int_0 = vec_0_int.rate_calculation();
		if (check_int_0 == answer_int_0){
			cout << "нахождение нормы (целые) - SUCCESS" << endl;
		}

		int scalar_int = vec_0_int.scalar_add(vec_1_int);
		if (scalar_int == answe_int_1) {
			cout << "скалярное умножение векторов (целые) - SUCCESS" << endl;
		}

		Vector<double> vec_2_double = vec_0_double + vec_1_double;
		if (vec_2_double == vec_answer_double_0) {
			cout << "сложение векторов (вещественные) - SUCCESS" << endl; 
		}

		Vector<double> vec_3_double = vec_0_double * sca_double;
		if (vec_3_double == vec_answer_double_1) {
			cout << "умножение вектора на скаляр (вещественные) - SUCCESS" << endl; 
		}

		double check_double_0 = vec_0_double.rate_calculation();
		if (check_double_0 == answer_double_0){
			cout << "нахождение нормы (вещественные) - SUCCESS" << endl;
		}

		double scalar_double = vec_0_double.scalar_add(vec_1_double);
		if (scalar_double == answe_double_1) {
			cout << "скалярное умножение векторов (вещественные) - SUCCESS" << endl;
		}

	}

int main() {
	int size;
	int flag = 3;
	int flag_1;
	int x;
	double y;
	ArraySequence<int> arr_int_0;
	ArraySequence<int> arr_int_1;
	ArraySequence<double> arr_double_0;
	ArraySequence<double> arr_double_1;
	while (flag != 0 ) {
		cout << "Лабораторная номер 2, вариант 4 (Вектор для целых и вещественных чисел)" << endl;
		cout << "\n";
		cout << "Если хотите выйти нажмите 0, если хотите запустить тест нажмите 1, если хотите продолжить введите любое число кроме 0 и 1";
		cout << "\n";
		cin >> flag;
		cout << "\n";

		if (flag == 0) {
			cout << "bye";
			cout << "\n";
			break;
		}

		if (flag == 1) {
			testing_methods();
			cout << "\n";
		}

		cout << "Введите размерность векторов" << endl;
		cin >> size;
		cout << endl;
		cout << "Если вы хотите работать в целых числах нажмите 1, если в вещественных нажмите 2" << endl;
		cin >> flag;
		cout << endl;
		if (flag == 1) {
			cout << "Введите координаты первого вектора" << endl;
			for (int i = 0; i < size; ++i) {
				cin >> x;
				arr_int_0.Prepend(x);
			}

			cout << "Введите координаты второго вектора" << endl;
			for (int j = 0; j < size; ++j) {
				cin >> x;
				arr_int_1.Prepend(x);
			}

			Vector<int> vec_0 (arr_int_0);
			Vector<int> vec_1 (arr_int_1);
			cout << endl;
			while (flag != 0) {
				main_menu();
				cout << "\n";
				cin >> flag;
				cout << "\n";

				if (flag == 3) {
					Vector<int> vec = vec_0 + vec_1;
					vec.Show_V();
					cout << "\n";
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 4) {
					int sca;
					int a; 
					cout << "Скаляр равен = ";
					cin >> sca;
					cout << "\n";
					cout << "Номер верктора равен = ";
					cin >> a;
					cout << "\n";

					if (a == 1) {
						Vector<int> vec = vec_0 * sca;
						vec.Show_V();
						cout << "\n";
					}
					if (a == 2) {
						Vector<int> vec = vec_1 * sca;
						vec.Show_V();
						cout << "\n";
					}

					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 5) {
					int a; 
					cout << "Номер верктора равен = ";
					cin >> a;
					cout << "\n";

					if (a == 1) {
						int norm = vec_0.rate_calculation();
						cout << "Норма вектора 1 равна = "<< norm << endl;
						cout << "\n";
					}
					if (a == 2) {
						int norm = vec_1.rate_calculation();
						cout << "Норма вектора 1 равна = " << norm << endl;
						cout << "\n";
					}
					
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 6) {
					int sca = vec_1.scalar_add(vec_0);
					cout << "Скалярное произведение векторов 1 и 2 = "<< sca << endl;
					cout << "\n";
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}
			}
		}

		if (flag == 2) {
			cout << "Введите координаты первого вектора" << endl;
			for (int i = 0; i < size; ++i) {
				cin >> y;
				arr_double_0.Prepend(y);
			}

			cout << "Введите координаты второго вектора" << endl;
			for (int j = 0; j < size; ++j) {
				cin >> y;
				arr_double_1.Prepend(y);
			}

			Vector<double> vec_0 (arr_double_0);
			Vector<double> vec_1 (arr_double_1);
			cout << endl;
			while (flag != 0) {
				main_menu();
				cout << "\n";
				cin >> flag;
				cout << "\n";

				if (flag == 3) {
					Vector<double> vec = vec_0 + vec_1;
					vec.Show_V();
					cout << "\n";
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 4) {
					double sca;
					int a; 
					cout << "Скаляр равен = ";
					cin >> sca;
					cout << "\n";
					cout << "Номер верктора равен = ";
					cin >> a;
					cout << "\n";

					if (a == 1) {
						Vector<double> vec = vec_0 * sca;
						vec.Show_V();
						cout << "\n";
					}
					if (a == 2) {
						Vector<double> vec = vec_1 * sca;
						vec.Show_V();
						cout << "\n";
					}

					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 5) {
					int a; 
					cout << "Номер верктора равен = ";
					cin >> a;
					cout << "\n";

					if (a == 1) {
						double norm = vec_0.rate_calculation();
						cout << "Норма вектора 1 равна = "<< norm << endl;
						cout << "\n";
					}
					if (a == 2) {
						double norm = vec_1.rate_calculation();
						cout << "Норма вектора 1 равна = " << norm << endl;
						cout << "\n";
					}
					
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}

				if (flag == 6) {
					double sca = vec_1.scalar_add(vec_0);
					cout << "Скалярное произведение векторов 1 и 2 = "<< sca << endl;
					cout << "\n";
					cout << "Введите 1 для продолжение работы" << endl;
					cin >> flag_1;
					if (flag_1 == 1) {
						cout << endl;
						cout << endl;
						cout << endl;
					}
				}
			}
		}
	}
	return 0;
}  